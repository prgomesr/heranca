package br.com.ozeano.heranca.repository;

import br.com.ozeano.heranca.model.SocioContribuinte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocioContribuinteRepository extends JpaRepository<SocioContribuinte, Long> {
}
