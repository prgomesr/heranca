package br.com.ozeano.heranca.repository;

import br.com.ozeano.heranca.model.FuncionarioPublico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FuncionarioPublicoRepository extends JpaRepository<FuncionarioPublico, Long> {
}
