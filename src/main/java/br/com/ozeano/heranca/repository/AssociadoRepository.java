package br.com.ozeano.heranca.repository;

import br.com.ozeano.heranca.model.Associado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssociadoRepository extends JpaRepository<Associado, Long> {
}
