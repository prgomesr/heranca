package br.com.ozeano.heranca.service;

import br.com.ozeano.heranca.model.SocioContribuinte;
import br.com.ozeano.heranca.repository.SocioContribuinteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SocioContribuinteService {

    @Autowired
    private SocioContribuinteRepository repository;

    public List<SocioContribuinte> listar() {
        return repository.findAll();
    }


}
