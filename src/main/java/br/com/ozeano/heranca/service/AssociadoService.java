package br.com.ozeano.heranca.service;

import br.com.ozeano.heranca.model.Associado;
import br.com.ozeano.heranca.model.FuncionarioPublico;
import br.com.ozeano.heranca.model.TipoAssociado;
import br.com.ozeano.heranca.repository.AssociadoRepository;
import br.com.ozeano.heranca.repository.FuncionarioPublicoRepository;
import br.com.ozeano.heranca.repository.SocioContribuinteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssociadoService {

    @Autowired
    private AssociadoRepository repository;

    @Autowired
    private FuncionarioPublicoRepository funcionarioPublicoRepository;

    @Autowired
    private SocioContribuinteRepository socioContribuinteRepository;

    public List<Associado> listar() {
        return repository.findAll();
    }

    public Associado cadastrar(Associado associado) {
        if (associado.getTipo().equals(TipoAssociado.FUNCIONARIO_PUBLICO)) {
            FuncionarioPublico funcionarioPublico = (FuncionarioPublico) associado;
            return funcionarioPublicoRepository.save(funcionarioPublico);
        }

        return repository.save(associado);
    }
}
