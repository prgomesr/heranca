package br.com.ozeano.heranca.service;

import br.com.ozeano.heranca.model.FuncionarioPublico;
import br.com.ozeano.heranca.repository.FuncionarioPublicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FuncionarioPublicoService {

    @Autowired
    private FuncionarioPublicoRepository repository;

    public FuncionarioPublico cadastrar(FuncionarioPublico funcionarioPublico) {
        return repository.save(funcionarioPublico);
    }

    public List<FuncionarioPublico> listar() {
        return repository.findAll();
    }

}
