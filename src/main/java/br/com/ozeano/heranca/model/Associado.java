package br.com.ozeano.heranca.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "id")
@JsonDeserialize(as = FuncionarioPublico.class)
public abstract class Associado extends PessoaFisica {

    @Column(name = "data_entrada")
    private LocalDate dataEntrada;

    @Column(name = "data_cancelamento")
    private LocalDate dataCancelamento;

    @Enumerated(EnumType.STRING)
    private TipoAssociado tipo;

    @Enumerated(EnumType.STRING)
    private Parentesco parentesco;

    private String foto;

    @ManyToOne
    @JoinColumn(name = "titular_id")
    @JsonIgnoreProperties("dependentes")
    private Associado titular;

    @JsonIgnoreProperties({"titular", "dependentes"})
    @OneToMany(mappedBy = "titular", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Associado> dependentes;

    public LocalDate getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(LocalDate dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public LocalDate getDataCancelamento() {
        return dataCancelamento;
    }

    public void setDataCancelamento(LocalDate dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }

    public Associado getTitular() {
        return titular;
    }

    public void setTitular(Associado titular) {
        this.titular = titular;
    }

    public List<Associado> getDependentes() {
        return dependentes;
    }

    public void setDependentes(List<Associado> dependentes) {
        this.dependentes = dependentes;
    }

    public TipoAssociado getTipo() {
        return tipo;
    }

    public void setTipo(TipoAssociado tipo) {
        this.tipo = tipo;
    }

    public Parentesco getParentesco() {
        return parentesco;
    }

    public void setParentesco(Parentesco parentesco) {
        this.parentesco = parentesco;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
