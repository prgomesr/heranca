package br.com.ozeano.heranca.model;

public enum TipoAssociado {
    SOCIO_CONTRIBUINTE, FUNCIONARIO_PUBLICO, DEPENDENTE_ESPECIAL
}
