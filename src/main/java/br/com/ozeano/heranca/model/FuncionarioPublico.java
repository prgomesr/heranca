package br.com.ozeano.heranca.model;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "id")
public class FuncionarioPublico extends Associado {

    @Column(name = "numero_rs")
    private String numeroRs;
    @Column(name = "numero_averbacao")
    private String numeroAverbacao;

    @ManyToOne
    @JoinColumn(name = "secretaria_id")
    private Secretaria secretaria;

    public String getNumeroRs() {
        return numeroRs;
    }

    public void setNumeroRs(String numeroRs) {
        this.numeroRs = numeroRs;
    }

    public String getNumeroAverbacao() {
        return numeroAverbacao;
    }

    public void setNumeroAverbacao(String numeroAverbacao) {
        this.numeroAverbacao = numeroAverbacao;
    }

    public Secretaria getSecretaria() {
        return secretaria;
    }

    public void setSecretaria(Secretaria secretaria) {
        this.secretaria = secretaria;
    }
}
