package br.com.ozeano.heranca.model;

public enum SituacaoPessoa {
    ATIVO, CANCELADO
}
