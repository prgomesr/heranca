package br.com.ozeano.heranca.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@PrimaryKeyJoinColumn(name = "id")
@Entity
public class SocioContribuinte extends Associado {
}
