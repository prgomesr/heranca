package br.com.ozeano.heranca.model;

public enum Parentesco {
    TITULAR, PAI, MAE, FILHO, CONJUGUE, ENTEADO
}
