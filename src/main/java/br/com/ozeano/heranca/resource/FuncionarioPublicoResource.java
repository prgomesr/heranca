package br.com.ozeano.heranca.resource;

import br.com.ozeano.heranca.model.FuncionarioPublico;
import br.com.ozeano.heranca.service.FuncionarioPublicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/funcionarios-publicos")
public class FuncionarioPublicoResource {

    @Autowired
    private FuncionarioPublicoService service;

    @GetMapping
    public List<FuncionarioPublico> listar() {
        return service.listar();
    }

    @PostMapping
    public ResponseEntity<FuncionarioPublico> criar(@RequestBody FuncionarioPublico funcionarioPublico) {
        FuncionarioPublico funcionarioPublicoSalvo = service.cadastrar(funcionarioPublico);
        return ResponseEntity.status(HttpStatus.CREATED).body(funcionarioPublicoSalvo);
    }

}
