package br.com.ozeano.heranca.resource;

import br.com.ozeano.heranca.model.SocioContribuinte;
import br.com.ozeano.heranca.service.SocioContribuinteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("socios-contribuintes")
public class SocioContribuinteResource {

    @Autowired
    private SocioContribuinteService service;

    @GetMapping
    public List<SocioContribuinte> listar() {
        return service.listar();
    }

}
