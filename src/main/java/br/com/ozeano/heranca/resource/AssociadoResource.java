package br.com.ozeano.heranca.resource;

import br.com.ozeano.heranca.model.Associado;
import br.com.ozeano.heranca.service.AssociadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/associados")
public class AssociadoResource {

    @Autowired
    private AssociadoService service;

    @GetMapping
    public List<Associado> listar() {
        return service.listar();
    }

    @PostMapping
    public ResponseEntity<Associado> criar(@RequestBody Associado associado) {
        Associado associadoSalvo = service.cadastrar(associado);
        return ResponseEntity.status(HttpStatus.CREATED).body(associadoSalvo);
    }
}
